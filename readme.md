#The More You Know

##Overview
Every time you open a new tab, you will learn something new. This takes the saying, "You lean something new every day" to a new level. You will also be met with a athletically pleasing simple design to keep you from being distracted from your work. Want to relax? Just stare off into the beautiful, hand picked backgrounds to keep you relaxed. Want to know the weather? No problem, just look in the bottom right hand corner!

##Known Issues
Yahoo Weather API depricated - Needs updating


##Questions about permissions?

Yahoo – Permission to get weather
