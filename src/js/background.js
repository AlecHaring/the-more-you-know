//Checks to make sure storage is correctly setup
function verifyStorage() {
    chrome.storage.local.get(["backgroundN"], function (items) {
        if (!items['backgroundN'] || items['backgroundN'] < 1) {
            n = 1
            chrome.storage.local.set({
                "backgroundN": n
            })
        }
    });

}

//Sends API Request
function getAPI() {
    widthArray = []
    heightArray = []
    //    Gets biggest screen computer
    chrome.system.display.getInfo(function (screens) {
        for (i = 0; i < screens.length; i++) {
            widthArray.push(screens[i].bounds.width)
            heightArray.push(screens[i].bounds.height)
        }
        screenWidth = Math.max.apply(null, widthArray)
        screenHeight = Math.max.apply(null, heightArray)
        $.ajax({
            dataType: "json",
            url: "https://alecharing.me/api/TMYK/api?type=pic&res=" + screenWidth + "x" + screenHeight,
            success: function (apiResponse) {
                if (apiResponse.success) {
                    updateAPIStorage(apiResponse);
                } else {
                    console.log("API Request failed");

                }
            }

        })
    })

}

//Takes info from API and sets it to storage
function updateAPIStorage(data) {
    if (data.pic.base64.length > 20) {
        chrome.storage.local.set({
            "fact": data.fact,
            "pic": data.pic,
            "lastAPIRequest": new Date().getTime(),

        }, function () {
            console.log("Done")
        });

    } else {
        console.log("Pic too short")

    }



}

//Runs verify storage and then API
function APIHandler() {
    $.when(verifyStorage()).then(function () {
        if ($.active === 0) {
            getAPI()

        }


    });

}

chrome.runtime.onMessage.addListener(
    function (request) {
        if (request.update == "openedTab") {
            APIHandler()

        }
    });

function updateWeather() {
    navigator.geolocation.getCurrentPosition(function (position) {
        posLat = position.coords.latitude;
        posLon = position.coords.longitude;
        jQuery.ajax({
            dataType: "json",
            url: "https://nominatim.openstreetmap.org/reverse?format=json&lat=" + posLat + "&lon=" + posLon + "&zoom=18&addressdetails=1",
            success: function (openstreetmapResponse) {
                addressObj = openstreetmapResponse.address;
                if (addressObj.hasOwnProperty('village')) {
                    weatherCity = addressObj.village;
                } else if (addressObj.hasOwnProperty('town')) {
                    weatherCity = addressObj.town;
                } else if (addressObj.hasOwnProperty('county')) {
                    weatherCity = addressObj.county;
                } else if (addressObj.hasOwnProperty('postcode')) {
                    weatherCity = addressObj.postcode;
                } else {
                    weatherCity = "Unknown";
                }
                console.log(weatherCity)
                $.ajax({
                    dataType: "json",
                    url: "https://alecharing.me/api/TMYK/getWeather?lat=" + posLat + "&lon=" + posLon,
                    success: function (apiResponse) {
                        if (apiResponse.currently) {
                            apiResponse.cityName = weatherCity;
                            chrome.storage.local.set({
                                "weatherData": apiResponse,

                            }, function () {
                                console.log("Updated Weather");
                            });
                        } else {
                            console.log("Weather update failed");

                        }
                    }

                })
            }

        })
    });

}

//Called on update or install
chrome.runtime.onInstalled.addListener(function (details) {
    if (details.reason == "install") {
        console.log("New Install");
        APIHandler();
    } else if (details.reason == "update") {
        var thisVersion = chrome.runtime.getManifest().version;
        console.log("Updated from " + details.previousVersion + " to " + thisVersion + "!");
    }
});


jQuery(document).ready(function () {
    updateWeather();
    setInterval(updateWeather, 1000 * 60 * 45);
})
