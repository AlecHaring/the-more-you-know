function updateTime() {
    date = new Date()
    var options = {
        hour: 'numeric',
        minute: 'numeric',
        hour12: true
    };
    var timeString = date.toLocaleString('en-US', options);
    AMPM = (timeString.indexOf("AM") != -1 ? "AM" : "PM")
    time = timeString.replace(" AM", "").replace(" PM", "")
    jQuery("#time").text(time)
    jQuery("#TOD").text(AMPM)
}

function setup() {
    updateTime()
    setInterval(updateTime, 1000);
    chrome.storage.local.get(["fact", "pic", "lastAPIRequest", "weatherData"], function (data) {

        //Debug
        console.log("Time since last successful API Request: " + new Date(data['lastAPIRequest']).toLocaleString())


        //Set variables
        fact = "Did you know " + data['fact']['fact']
        summary = data['fact']['summary'];
        picBase64 = data['pic']['base64']
        picLocation = data['pic']['location'];
        photo_credit = data['pic']['photo_credit'];
        credit_page = data['pic']['credit_page'];
        credit_page_type = data['pic']['credit_page_type'];

        //Set from storage
        jQuery(".link-container a").attr("href", data['fact']['url'])
        jQuery(".fact").text(fact)
        jQuery("#sec1-bg").attr("data-id", "picID_" + data['pic']['id'])
        jQuery("#fact").attr("data-id", "factID_" + data['fact']['id'])
        jQuery("#sec1-bg").css("background-image", "url(" + picBase64 + ")")
        jQuery("#sec1-bg").css("transform", "scale(1.1)")

        //If pic has location
        if (picLocation) {
            jQuery(".picTitle").text(picLocation)
            jQuery(".picCredit").text("Photo by " + photo_credit)
        } else { //instead of showing location, show photographer's name
            jQuery(".picTitle").text("Photo by " + photo_credit)
            jQuery(".picCredit").text("View Portfolio")
        }

        //If fact source has a summary
        if (summary) {
            summary = summary.split("\n");
            for (i = 0; i < summary.length; i++) {
                if (summary[i]) {
                    jQuery('#summaryList').append("<li>" + summary[i] + "</li>")
                }

            }

        }

        //Find proper url format -- ex. insta needs url before username
        switch (credit_page_type) {
            case "instagram":
                jQuery(".picCredit").click(function () {
                    window.open('https://instagram.com/' + credit_page, '_blank');

                })
                break;
            case "portfolio":
                jQuery(".picCredit").click(function () {
                    window.open(credit_page, '_blank');

                })
                break;
            case "unsplash":
                jQuery(".picCredit").click(function () {
                    window.open(credit_page, '_blank');

                })
        }

        //Set weatherData
        currentWeather = data['weatherData']['currently']
        var icons = new Skycons({
            "color": "rgba(255, 255, 255)"
        })
        icons.set("weatherCanvas", currentWeather.icon);
        icons.play();
        jQuery("#weatherLocation").text(data['weatherData'].cityName)
        jQuery(".temperature").html(Math.round(currentWeather.temperature) + '&#176;F')




    })
}

function randomBGEffect() {
    switch (Math.floor(Math.random() * 3) + 1) {
        case 1:
            jQuery('.sec_2').append('<canvas id="canvas1" class="bg"></canvas> <canvas id="canvas2" class="bg"></canvas> <canvas id="canvas3" class="bg"></canvas>')
            activateRainEffect()
            console.log("Current Effect: Rain")
            break;
        case 2:
            jQuery('.sec_2').append('<canvas id="sec2-bg" width="100%" height="100%"></canvas>')
            activateHyperspaceEffect()
            console.log("Current Effect: Hyperspace")
            break;
        case 3:
            jQuery('.sec_2').append('<canvas id="sec2-bg" width="100%" height="100%"></canvas>')
            activateShootingStarEffect()
            console.log("Current Effect: Shooting Star")
    }


}


function checkInternet() {


}
//Initialize

jQuery(document).ready(function () {
    randomBGEffect()

    setup()

    chrome.runtime.sendMessage({
        update: "openedTab"
    });

    new fullpage('#main-view', {
        //options here
        autoScrolling: true,
        licenseKey: 'a4(1YC g6V}+scN;:n=Vw+*9c2TE6jGAE~y5$7%#I,ue=0RR26c|9Yo0=',
    });

    jQuery(".scroll-indicator").click(function () {
        fullpage_api.moveSectionDown()

    })

    jQuery("body").fadeIn(300)



})
