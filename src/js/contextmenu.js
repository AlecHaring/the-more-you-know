$(document).ready(function () {

    // Trigger action when the contexmenu is about to be shown
    $("#sec1-bg").contextmenu(function (event) {

        // Avoid the real one
        event.preventDefault();

        // Avoid the event from bubbling up to parent
        event.stopPropagation();

        // Show contextmenu
        $("#background-contextmenu").finish().toggle(100).

        // In the right position (the mouse)
        css({
            top: event.pageY + "px",
            left: event.pageX + "px"
        });
    });
    // Trigger action when the contexmenu is about to be shown
    $("#fact").contextmenu(function (event) {

        // Avoid the real one
        event.preventDefault();

        // Avoid the event from bubbling up to parent
        event.stopPropagation();

        // Show contextmenu
        $("#fact-contextmenu").finish().toggle(100).

        // In the right position (the mouse)
        css({
            top: event.pageY + "px",
            left: event.pageX + "px"
        });
    });


    // If the document is clicked somewhere
    $(document).click(function (e) {

        // If the clicked element is not the menu
        if (!$(e.target).parents(".custom-menu").length > 0) {

            // Hide it
            $(".custom-menu").hide(100);
        }
    });


    // If the menu element is clicked
    $("#background-contextmenu li").click(function () {
        id = $("#sec1-bg").attr("data-id").replace("picID_", "")
        // This is the triggered action name
        switch ($(this).attr("data-action")) {

            // A case for each action. Your actions here
            case "approve":
                $.ajax({
                    dataType: "json",
                    url: "https://alecharing.me/api/TMYK/api?type=change&changeType=changePic&id=" + id + "&markAs=1",
                    success: function (apiresponse) {
                        console.log("Approved: " + id)
                    }

                })
                break;
            case "delete":
                $.ajax({
                    dataType: "json",
                    url: "https://alecharing.me/api/TMYK/api?type=change&changeType=changePic&id=" + id + "&markAs=2",
                    success: function (apiresponse) {
                        console.log("Deleted: " + id)
                    }
                })
                break;
            case "hardRead":
                $.ajax({
                    dataType: "json",
                    url: "https://alecharing.me/api/TMYK/api?type=change&changeType=changePic&id=" + id + "&markAs=3",
                    success: function (apiresponse) {
                        console.log("Hard to read: " + id)
                    }

                })
        }

        // Hide it AFTER the action was triggered
        $(".custom-menu").hide(100);
    });

    $("#fact-contextmenu li").click(function () {
        id = $("#fact").attr("data-id").replace("factID_", "")
        // This is the triggered action name
        switch ($(this).attr("data-action")) {

            // A case for each action. Your actions here
            case "approve":
                $.ajax({
                    dataType: "json",
                    url: "https://alecharing.me/api/TMYK/api?type=change&changeType=changeFact&id=" + id + "&markAs=1",
                    success: function (apiresponse) {
                        console.log("Approved: " + id)
                    }

                })
                break;
            case "edit":
                var editFact = prompt("Edit fact", jQuery("#fact").text());
                if (editFact != null) {
                    var approved = confirm("Want to edit and approve this fact?")
                    if (approved) {
                        jQuery("#fact").text(editFact)
                        $.ajax({
                            dataType: "json",
                            url: "https://alecharing.me/api/TMYK/api?type=change&changeType=editFact&updatedFact=" + editFact + "&id=" + id + "&markAs=1",
                            success: function (apiresponse) {
                                console.log("Edited: " + id)
                            }

                        })
                    }
                }
                break;
            case "delete":
                $.ajax({
                    dataType: "json",
                    url: "https://alecharing.me/api/TMYK/api?type=change&changeType=changeFact&id=" + id + "&markAs=2",
                    success: function (apiresponse) {
                        console.log("Deleted: " + id)
                    }
                })
        }

        // Hide it AFTER the action was triggered
        $(".custom-menu").hide(100);
    });

});
